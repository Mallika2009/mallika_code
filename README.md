# Template Viewer

## Description
This application displays the thumbnail images in a filmstrip view below the main large view.By default 4 thumbnail images are displayed. Every next button click fetches another set of images till the end of reference data. Previous brings in the previous images.Selecting any images displays the image in large section and displays the image attributes. Previous and next buttons are disabled when there is no data to show. 

### Assumption
When page loads by default first  image will be displayed in large image view section and corresponding meta data will be displayed 

## Installation 
* Clone the git repository 
* Go to the project directory 
* Run npm install
* Run npm start 
* Go to localhost:3000 from browser

## Live Link 
[TemplateViewer Live Link](https://safe-tundra-82509.herokuapp.com/)

## Technology
* React 16.8
* Express
* Node
* JavaScript
* CSS
* HTML
* Axios

## Authors and acknowledgment
* Mallika Chakraborty

## Sample Views

![Default View](./client/public/images/screenshot1.PNG)


