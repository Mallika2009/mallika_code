/*
==================================================
 @<COPYRIGHT>@
==================================================
 File description:
    This is node server setup file

    File   : server.js
    Module : server

=============================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=============================================================*/
//import express npm package in node server
const express = require("express");
const path = require("path");

const PORT = process.env.PORT || 3001;

const app = express();
const apiRoutes = require("./routes/apiRoutes");

// Define middleware here
//Bind application level middleware to an instance using use()
// Sets up the Express app to handle data parsing
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// Serve up static assets (usually on heroku)
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}

// Use apiRoutes
app.use("/api", apiRoutes);

app.listen(PORT, function() {
  console.log(`🌎 ==> API server now on port ${PORT}!`);
});