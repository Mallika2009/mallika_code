import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// Create React App, an environment that comes pre-configured with everything you need to build a React app.
// It will create a live development server, use Webpack(module bundler) to automatically 
// compile React, JSX, and ES6, auto-prefix CSS files, minify for production and use ESLint to test and warn 
// about mistakes in the code.webpack uses bable to compile ES6 into a backwards compatible version of JavaScript.
//Render a app (react element) into the DOM in the supplied container i.e. root
ReactDOM.render(<App />, document.getElementById("root"));

