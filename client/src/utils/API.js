/*
==================================================
 @<COPYRIGHT>@
==================================================
 File description:
    This file contains rest api call to get the image data from the server.

    File   : src/API.js
    Module : Client 

=============================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=============================================================*/
import axios from "axios";
//Axios is a promise-based HTTP client that supports an 
//easy-to-use API and can be used in both the browser and Node.js
// The getThumbnailImages method retrieves images from the server
// It accepts offset and limit parameters to get the images.
export default {
  getThumbnailImages: function(offset, limit) {
    return axios.get("/api/thumbnailimages/" + offset + "/" +  limit);
  }
};
