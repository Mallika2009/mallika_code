/*
==================================================
 @<COPYRIGHT>@
==================================================
 File description:
    This file contains main function component written using react hooks.Hooks lets developers 
    to use state and lifecycle hooks without using a class component. 
    This is the high level component and passes the state to the lower level components.
    This component mailtains the state changes uning useState hook.
    React hook used are useState, useEffect, useCallback, useMemo .
 
    File   : main/index.js
    Component : Main

=============================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=============================================================*/

//Import for React module , react hooks and custom function components
import React, { useState, useEffect, useCallback, useMemo } from "react";
import ThumbnailsView from "../thumbnailview"
import LargeImageView from "../largeview"
import ImageAttributeView from "../attrview"
import * as Constant  from "../../constants"
import API from "../../utils/API"

function Main() {
  // =============================================================================
  // useState is a React Hook returns a pair: the current state value and a function  
  // to update it
  // React will preserve this state between re-renders.
  // it doesn’t merge the old and new state together.
  // =============================================================================
  const [imageArr, setImageArr] = useState([]);
  const [next, setnext] = useState(false);
  const [selectedImg, setSelectedImg] = useState("");
  const [offset, setOffset] = useState(0);

  // =============================================================================
  // This a generic function calls when offset state variable changes on previous and next 
  // button click. Indside it calls a rest API and gets the image data from server
  // Output: No of image data based on Limit. 
  // Example: If Limit is 4 and offset is 0,then first four Thumbnail images display.
  // async function with an await expression that pauses the execution of the 
  // async function and waits for the passed Promise's resolution, and then resumes the 
  // async function's execution and evaluates as the resolved value.
  // An asynchronous function is a function which operates asynchronously via the event loop, 
  // using an implicit Promise to return its result.
  // =============================================================================
  const  displayThumbnailImages = async (offset, count, flag) =>{
        const res = await API.getThumbnailImages(offset,count)
        setImageArr(res.data.images)
        setnext(res.data.msg)
        if(flag){
          let tempImageArr = res.data.images
          setSelectedImg(tempImageArr[0])
        }
  } 
  // =============================================================================
  // react useEffect() hook calls web page on load. Same as componentDidMount() lifecycle 
  // method. Essentially fetches data from server using a rest api call. 
  // Defaul gets four thumbnail images and offset starts at 0
  // Output: thumbnail images, error msg(if any) and default large image
  // =============================================================================
  useEffect(() =>{
   displayThumbnailImages(0, Constant.LIMIT,true)
  }, [])

  // =============================================================================
  // The default behavior for effects is to fire the effect after every completed render.
  // Here useEffect() renders conditionally
  // That way effect will only recreate when offset changes.
  // =============================================================================
  useEffect(() =>{
     displayThumbnailImages(offset, Constant.LIMIT,false)
  }, [offset])

  // =============================================================================
  // useCallback() returns a memoized callback that only changes if one of the dependencies 
  // has changed.This prevent unnecessary child component rendering (thumbnailview).
  // react useCallBack() hook calls when a thumbnail image is selected. 
  // It gets the id of the selected image and then find that image from the imageArr. 
  // and then updates the selectedImg state variable 
  // shouldComponentUpdate lifecycle method
  // =============================================================================
  const handleImgClick = useCallback((e) => {
    let imgId = e.target.id
    let newArray = [...imageArr].filter(x => x.id === imgId)
    setSelectedImg(newArray[0])
  }, [imageArr])

  // =============================================================================
  // useMemo() returns a memoized value that only changes if one of the dependencies 
  // has changed.This optimization helps to avoid expensive calculations on every render.
  // Function passed (i.e. LargeImgaeView ) to useMemo runs during rendering. 
  // If no array is provided, a new value will be computed on every render.
  // Similar to shouldComponentUpdate lifecycle method
  // =============================================================================
  const largeView = useMemo(() => <LargeImageView image={selectedImg.image} 
                    altText="Large"
                    width="430"
                    height="360"
                     />, [selectedImg])

  const attrView = useMemo(() => <ImageAttributeView title={selectedImg.title}
                    description={selectedImg.description}
                    cost={selectedImg.cost}
                    id={selectedImg.id}
                    thumbnail={selectedImg.thumbnail}
                    image={selectedImg.image} />, [selectedImg])

  //return react element
  return (
    <div id="main" role="main">
      <div id="large"> 
     		<div className="group">
          {/* renders when selected image changes */}
           {largeView}
           {attrView}
     		</div>
     	</div> 
       {/* renders the thumbnail views based on the image value in inagArr */}
      <div className="thumbnails">
        <div className="group"> 
        {imageArr.map( img => 
                          <ThumbnailsView
                            key={img.id}
                            thumbnail={img.thumbnail}
                            id={img.id}
                            width="145"
                            height="121"
                            onClick={handleImgClick}
                            />
                        )}
          {/* offset value changes on previous and next button click 
          If no image left then next button gets disabled. same with previous*/}
          <button  className={"previous " + (!offset? "disabled" : "enabled")} onClick={() => setOffset(offset - Constant.LIMIT)} disabled={!offset }></button>
          <button className={"next " + (next? "disabled" : "enabled")}  onClick={() => setOffset(offset + Constant.LIMIT)} disabled={next}>Next</button>
        </div>
      </div>
    </div>
  );
}
export default Main