/*
===================================================================
 @<COPYRIGHT>@
==================================================================
 File description:
    This file contains Header functional component.
    Functional components are easier to read and test 
    (plain JavaScript functions without 
    state or lifecycle-hooks),less code,best practices for container 
    and presentational components,chance of having a performance boost in future
    This component renders from App component
    File   : header/index.js
    Component : Header

=================================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=================================================================*/
import React from 'react'
const  Header = () => { 
    return(  
        <header data-test='headerComponent'>
		    Code Development Project
        </header>
    ) 
}
export default Header;
    