/*
==================================================
 @<COPYRIGHT>@
==================================================
 File description:
    This file contains LargeImageView functional component.
    This component diplays the large image of the selected image 

    File   : largeview/index.js
    Component : LargeImageView

=============================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=============================================================*/

import React, {memo} from "react"
//same as React.PureComponent but for function components instead of classes.
//if your function component renders the same result given the same props, 
//you can wrap it in a call to React.memo for a performance boost in some 
//cases by memoizing the result. This means that React will skip rendering the component, 
//and reuse the last rendered result.
const LargeImageView = memo((props) =>{
    //propf destructuring to make code more readable and clear
    const {image,altText,width,height} = props
    if(!image){
        return null
    }
    return(  
        <img src={`images/large/${image}`}  data-test="largeViewComponent"
             alt={altText}
             width={width} 
             height={height} 
        />
    ) 
})
export default LargeImageView;

