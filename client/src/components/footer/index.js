/*
==================================================
 @<COPYRIGHT>@
==================================================
 File description:
    This file contains Footer functional component.
    For class components every render there will be an instance created and lot of code a
    after compilation. so performence issue 
    File   : footer/index.js
    Component : Footer

=============================================================
 date            name                  description of change

 07/23/2019      Mallika Chakraborty    Initial creation
=============================================================*/
import React from "react"
const Footer = () => {
    return(  
        <footer data-test="footerComponent">
		    <a href="instructions.pdf">Download PDF Instructions</a>
        </footer>
    ) 
}
export default Footer;

